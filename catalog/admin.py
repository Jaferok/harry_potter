from django.contrib import admin
from .models import Author, Genre, Book, BookInstance, Language, Merchandise, Attachment, Property, PropertyImage
from core.models import Sales, Review

# admin.site.register(Book)
# admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Language)
admin.site.register(Sales)
admin.site.register(Attachment)
admin.site.register(Review)
# admin.site.register(BookInstance)


class PropertyImageInline(admin.TabularInline):

    model = PropertyImage
    extra = 3


class PropertyAdmin(admin.ModelAdmin):

    inlines = [PropertyImageInline, ]


admin.site.register(Property, PropertyAdmin)


class BooksInstanceInline(admin.TabularInline):

    extra = 0
    model = BookInstance


class BooksInline(admin.TabularInline):

    extra = 0
    model = Book


class AuthorAdmin(admin.ModelAdmin):

    list_display = ('last_name', 'first_name', 'date_of_birth', 'date_of_death')
    fields = ['first_name', 'last_name', ('date_of_birth', 'date_of_death')]
    inlines = [BooksInline]


admin.site.register(Author, AuthorAdmin)


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):

    list_display = ('title', 'author', 'display_genre', 'language')
    inlines = [BooksInstanceInline]


@admin.register(BookInstance)
class BookInstanceAdmin(admin.ModelAdmin):

    list_display = ('book', 'status', 'borrower', 'due_back', 'id')

    list_filter = ('status', 'due_back')

    fieldsets = (
        (None, {
            "fields": (
                'book', 'imprint', 'id'
            ),
        }),
        ('Availability', {
            'fields': (
                'status', 'due_back', 'borrower'
            ),
        }),
    )


@admin.register(Merchandise)
class MerchandiseAdmin(admin.ModelAdmin):

    list_display = ('title', 'price', 'description')

