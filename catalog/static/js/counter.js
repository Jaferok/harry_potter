// counter function
var a = 0;
function counter(elem) {
    $(elem).each(function(){
    a++;
    $(this).attr("id", "count_product_detail" + a);
    var thisId = $(this).attr("id");
    $(this).siblings('.plus_detail').attr('href', "#" + thisId);
    $(this).siblings('.minus_detail').attr('href', "#" + thisId);
    });
}
counter('.count_product_detail');

$('.minus_detail').on('click', function(e){
    e.preventDefault();
    var linkHref = $(this).attr('href');
    var inputElem = $(linkHref);
    var elemVal = inputElem.val();
    var price = $(this).parent(".count_product_input").parent(".navigation_count_product").prev('.price_product').find('.price').text();
    var summa_like = $(this).parent(".count_product_input").parent(".navigation_count_product").next('.sum_box').find('.summa_like');
    elemVal--;
    $(this).siblings(linkHref).val("" + elemVal);
    if (elemVal <= 0) {
        $(this).siblings(linkHref).val("" + 0)
    }
    summa_like.text(price * elemVal);
    inputElem.change();
    summa_like.change();
})

$('.plus_detail').on('click', function(e){
    e.preventDefault();
    var linkHref = $(this).attr('href');
    var price = $(this).parent(".count_product_input").parent(".navigation_count_product").prev('.price_product').find('.price').text();
    var summa_like = $(this).parent(".count_product_input").parent(".navigation_count_product").next('.sum_box').find('.summa_like');
    var inputElem = $(linkHref);
    var elemVal = inputElem.val();
    elemVal++;
    $(this).siblings(linkHref).val("" + elemVal);
    summa_like.text(price * elemVal);
    inputElem.change();
    summa_like.change();
})
