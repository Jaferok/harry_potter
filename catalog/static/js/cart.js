$(".plus").on("click", function(){

    var elem = $(this);

    $.ajax({
        url: elem.attr("data-url"),
        data : {
                    "csrfmiddlewaretoken" : $(".plus").siblings("input[name='csrfmiddlewaretoken']" ).val(),
                    "count": "1",
                    "pk": elem.attr('pk'),
                    "quantity": elem.parent(".navigation_count_product").children("#count_product").val(),
                    "merch": elem.parent(".navigation_count_product").children(".merchandise").val(),
                },
        method: "POST",
        dataType: "json",
        success: function(data) {
                if(data.is_success) {
                    elem.parent(".navigation_count_product").children(".count_product").val(data.count);
                    elem.parent(".navigation_count_product").next(".sum_box").children(".summa").text("$" + data.item_total_price);
                    $(".summa_all").html("$" + data.summary);
                    $(".items_number").text("В корзине находится " + data.items_number + " предметов");
                }
                else {
                    elem.parents(".navigation_count_product").children('.count_product').val("biba")
                }
        }
    });
});

$(".minus").on("click", function(){

    var elem = $(this);

    $.ajax({
        url: elem.attr("data-url"),
        data : {
                    "csrfmiddlewaretoken" : $(".minus").siblings("input[name='csrfmiddlewaretoken']" ).val(),
                    "count": "1",
                    "pk": elem.attr('pk'),
                    "quantity": elem.parent(".navigation_count_product").children("#count_product").val(),
                    "merch": elem.parent(".navigation_count_product").children(".merchandise").val(),
                },
        method: "POST",
        dataType: "json",
        success: function(data) {
                if(data.is_success) {
                    elem.parent(".navigation_count_product").children(".count_product").val(data.count);
                    elem.parent(".navigation_count_product").next(".sum_box").children(".summa").text("$" + data.item_total_price);
                    $(".summa_all").html("$" + data.summary);
                    $(".items_number").text("В корзине находится " + data.items_number + " предметов");

                }
                else {
                    elem.parents(".navigation_count_product").children('.count_product').val("biba")
                }
        }
    });
});