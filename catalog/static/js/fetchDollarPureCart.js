$(document).ready(function() {
  $.ajax({
    type: "GET",
    url: "https://cors-anywhere.herokuapp.com/" + "http://nsp.com.ru",
    success: function(data) {
      var index_dollar = data.indexOf("у.е");
      value_dollar = data.substring(index_dollar + 10, index_dollar + 15);
    },
    error: function() {
      console.log(
        "Произошла ошибка получения данных, обратитесь к программисту."
      );
    }
  });
  setInterval(function() {
    var prices = document.getElementsByClassName("product-item-price-current");
    var total = document.getElementsByClassName(
      "product-item-amount-description-container"
    );

    try {
      var detail = document.getElementsByClassName(
        "product-item-detail-price-current"
      );
      if (detail) {
        for (i = 0; i < detail.length; i++) {
          if (detail[i].textContent.trim().indexOf("$") === 0) {
            var old_price = detail[i].textContent.trim().replace(/[$,]/g, "");
            var new_price = Number(old_price) * Number(window.value_dollar);
            var fixedNewPrice = new_price.toFixed(2) + " руб.";
            detail[i].textContent = fixedNewPrice;
          }
        }
      }
    } catch (error) {}

    try {
      var cart = document.getElementsByClassName("bx-basket-block");
      if (cart) {
        if (
          cart[0].children[3].children[0].textContent.trim().indexOf("$") === 0
        ) {
          var old_price = cart[0].children[3].children[0].textContent
            .trim()
            .replace(/[$,]/g, "");
          var new_price = Number(old_price) * Number(window.value_dollar);
          var fixedNewPrice = new_price.toFixed(2) + " руб.";
          cart[0].children[3].children[0].textContent = fixedNewPrice;
        }
      }
    } catch (error) {}

    try {
      var search_results = document.getElementsByClassName("bx_item_block");
      if (search_results) {
        for (i = 0; i < search_results.length; i++) {
          if (
            search_results[i].children[1].children[1].textContent
              .trim()
              .indexOf("$") === 0
          ) {
            var old_price = search_results[
              i
            ].children[1].children[1].textContent
              .trim()
              .replace(/[$,]/g, "");
            var new_price = Number(old_price) * Number(window.value_dollar);
            var fixedNewPrice = new_price.toFixed(2) + " руб.";
            search_results[
              i
            ].children[1].children[1].textContent = fixedNewPrice;
          }
        }
      }
    } catch (error) {}

    try {
      var order_total_product = document.getElementsByClassName("bx-price all");
      if (order_total_product) {
        for (i = 0; i < order_total_product.length; i++) {
          if (order_total_product[i].textContent.trim().indexOf("$") === 0) {
            var old_price = order_total_product[i].textContent
              .trim()
              .replace(/[$,]/g, "");
            var new_price = Number(old_price) * Number(window.value_dollar);
            var fixedNewPrice = new_price.toFixed(2) + " руб.";
            order_total_product[i].textContent = fixedNewPrice;
          }
        }
      }
    } catch (error) {}

    try {
      var order_total = document.getElementsByClassName("bx-soa-cart-d");
      if (order_total) {
        for (i = 0; i < order_total.length; i++) {
          if (order_total[i].textContent.trim().indexOf("$") === 0) {
            var old_price = order_total[i].textContent
              .trim()
              .replace(/[$,]/g, "");
            var new_price = Number(old_price) * Number(window.value_dollar);
            var fixedNewPrice = new_price.toFixed(2) + " руб.";
            order_total[i].textContent = fixedNewPrice;
          }
        }
      }
    } catch (error) {}

    try {
      var order_totals = document.getElementsByClassName("bx-soa-cart-d bx-soa-changeCostSign");
      if (order_totals) {
          if (order_totals[0].textContent.trim().indexOf("$") === 0) {
            var old_price = order_totals[0].textContent
              .trim()
              .replace(/[$,]/g, "");
            var new_price = Number(old_price) * Number(window.value_dollar);
            var fixedNewPrice = new_price.toFixed(2) + " руб.";
            order_totals[0].textContent = fixedNewPrice;
          }
      }
    } catch (error) {}

    for (i = 0; i < prices.length; i++) {
      if (prices[i].textContent.trim().indexOf("$") === 0) {
        var old_price = prices[i].textContent.trim().replace(/[$,]/g, "");
        var new_price = Number(old_price) * Number(window.value_dollar);
        var fixedNewPrice = new_price.toFixed(2) + " руб.";
        prices[i].textContent = fixedNewPrice;
      }
    }

    for (i = 0; i < total.length; i++) {
      try {
        total[i].children[1].children[0].textContent.trim().indexOf("$");
        if (
          total[i].children[1].children[0].textContent.trim().indexOf("$") === 0
        ) {
          var old_price = total[i].children[1].children[0].textContent
            .trim()
            .replace(/[$,]/g, "");
          var new_price = Number(old_price) * Number(window.value_dollar);
          var fixedNewPrice = new_price.toFixed(2) + " руб.";
          total[i].children[1].children[0].textContent = fixedNewPrice;
        }
      } catch (error) {
        continue;
      }
    }
  }, 4500);
});
