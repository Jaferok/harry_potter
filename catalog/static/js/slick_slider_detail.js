$(document).ready(function() {
    $('.slider_item-main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        asNavFor: '.slider_item-nav'
    });
    $('.slider_item-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: false,
        asNavFor: '.slider_item-main',
        dots: false,
        arrows: true,
        focusOnSelect: true
    });
})