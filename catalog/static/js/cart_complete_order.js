var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form ...
    var i, x = document.getElementsByClassName("tab");
    var prevBtns = document.getElementsByClassName("prevBtn");
    var nextBtns = document.getElementsByClassName("nextBtn");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        for (i = 0; i < prevBtns.length; i++) {
            
            prevBtns[i].style.display = "none";
        };
    } else {
        for (i = 0; i < prevBtns.length; i++) {
            
            prevBtns[i].style.display = "inline";
        };
    }
    if (n == (x.length - 1)) {
        for (i = 0; i < nextBtns.length; i++) {
            
            nextBtns[i].innerHTML = "Отправить";
        };
    } else {
        for (i = 0; i < nextBtns.length; i++) {
            
            nextBtns[i].innerHTML = "Далее";
        };
    }
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var i,x = document.getElementsByClassName("tab");
    var radio_buttons = x[currentTab].getElementsByClassName("payment-radio");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm() && radio_buttons.length > 0) {

        for (i = 0; i < radio_buttons.length; i++) {
                radio_buttons[i].className += " error";
            }
            return false;
    } else if(n == 1 && !validateForm()) {
        return false;
    }
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    
    if (currentTab == 2) {
        completeOrder();
    } else if (currentTab == 1) {
        blockInputs();
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function completeOrder() {
    var all_deliveries = ["Курьерская доставка в пределах МКАД и КАД", "Доставка службой CDEK"];
    var name = document.getElementById("fio").value;
    var tel = document.getElementById("phone").value;
    var address = document.getElementById("address").value;
    var comment = document.getElementById("comment").value;
    var delivery = document.getElementsByClassName("delivery_list_item active")[0].getElementsByClassName('open_link')[0].textContent;
    var payment_img = document.getElementsByClassName("payment_list_item_active")[0].getElementsByTagName("img")[0].outerHTML;
    var payment_text = document.getElementsByClassName("payment_list_item_active")[0].getElementsByTagName("p")[1].textContent;

    document.getElementById("complete_fio").textContent = name;
    document.getElementById("complete_tel").textContent = tel;
    document.getElementById("complete_address").textContent = address;
    document.getElementsByClassName("your_payment_img")[0].innerHTML = payment_img;
    document.getElementsByClassName("your_payment_text")[0].innerText = payment_text;

    if (all_deliveries.indexOf(delivery) != -1) {
        document.getElementById("complete_delivery").textContent = delivery;
    } else {
        document.getElementById("complete_delivery").textContent = "Самовывоз";
    }
}

function blockInputs() {

    var all_deliveries = ["Курьерская доставка в пределах МКАД и КАД", "Доставка службой CDEK"];

    var delivery = document.getElementsByClassName("delivery_list_item active")[0].getElementsByClassName('open_link')[0].textContent;

    if (all_deliveries.indexOf(delivery) != -1) {
        document.getElementById("address").removeAttribute("disabled");
    } else {
        document.getElementById("address").setAttribute("disabled", "disabled");
        document.getElementById("address").value = delivery;
    }
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, radio, valid = true;
    var check = 0;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    radio = document.getElementsByClassName("payment-radio");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " error";
            // and set the current valid status to false:
            valid = false;
        }
    };

    for (i = 0; i < radio.length; i++) {

        if (radio[i].checked == true) {
            check += 1;
        }
    };

    if (check != 1) {
        valid = false;
    };

    return valid; // return the valid status
}


$(".radio_custom").click(function(){

    var radio_buttons, i;
    radio_buttons = document.getElementsByClassName("payment-radio");

    for (i = 0; i < radio_buttons.length; i++) {

        if (radio_buttons[i].classList.contains('error')){
            radio_buttons[i].classList.remove('error');
        }
    }
});

$(".form-control").keydown(function(event) {
    if (event.which == 13){
        event.preventDefault();
        $("#nextBtn").click();
    }
});

$(document).ready(function(){

    var elem = $(this);
    var cart_summary = elem.find(".cart_summary").text();
    var delivery_price = elem.find(".delivery_price").text();
    var total_price = String(Number(cart_summary) + Number(delivery_price));

    elem.find(".cart_summary").text(total_price);
    // elem.find(".cart_summary").change();

});