$(document).ready(function(){
    $('.slider').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
{
  breakpoint: 1190,
  settings: {
    slidesToShow: 3,
    slidesToScroll: 3,
  }
},
{
  breakpoint: 1000,
  settings: {
    slidesToShow: 2,
    slidesToScroll: 2
  }
},
{
  breakpoint: 480,
  settings: {
    slidesToShow: 1,
    slidesToScroll: 1
  }
}
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
    });
});