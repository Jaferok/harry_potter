$(document).ready(function() {
  setTimeout(function() {
    var item_price = document.getElementsByClassName(
      "basket-item-price-current-text"
    );
    for (i = 0; i < item_price.length; i++) {
      if (item_price[i].textContent.trim().indexOf("$") === 0) {
        var old_price = item_price[i].textContent.trim().replace(/\$/g, "");
        var new_price = Number(old_price) * Number(window.value_dollar);
        var fixedNewPrice = new_price.toFixed(2) + " руб.";
        item_price[i].textContent = fixedNewPrice;
      }
    }
    var summary = document.getElementsByClassName(
      "basket-coupon-block-total-price-current"
    );
    for (i = 0; i < summary.length; i++) {
      if (summary[i].textContent.trim().indexOf("$") === 0) {
        var old_price = summary[i].textContent.trim().replace(/[$,]/g, "");
        var new_price = Number(old_price) * Number(window.value_dollar);
        var fixedNewPrice = new_price.toFixed(2) + " руб.";
        summary[i].textContent = fixedNewPrice;
      }
    }
  }, 6000);

    setInterval(function() {
      var prices = document.getElementsByClassName(
        "basket-item-price-current-text"
      );
      var summary = document.getElementsByClassName(
        "basket-coupon-block-total-price-current"
      );

      for (i = 0; i < prices.length; i++) {
        if (prices[i].textContent.trim().indexOf("$") === 0) {
          var old_price = prices[i].textContent.trim().replace(/[$,]/g, "");
          var new_price = Number(old_price) * Number(window.value_dollar);
          var fixedNewPrice = new_price.toFixed(2) + " руб.";
          prices[i].textContent = fixedNewPrice;
        }
      }

      for (i = 0; i < summary.length; i++) {
        if (summary[i].textContent.trim().indexOf("$") === 0) {
          var old_price = summary[i].textContent.trim().replace(/[$,]/g, "");
          var new_price = Number(old_price) * Number(window.value_dollar);
          var fixedNewPrice = new_price.toFixed(2) + " руб.";
          summary[i].textContent = fixedNewPrice;
        }
      }
    }, 2700);
});
