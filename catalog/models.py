from django.contrib.auth.models import User
from django.urls import reverse  # Для генерации URL переворачиванием URL паттернов
from django.db import models
from datetime import date
import uuid


class Property(models.Model):

    address = models.TextField()

    def __str__(self):

        return self.address


class PropertyImage(models.Model):

    property = models.ForeignKey(Property, related_name="images", on_delete=models.SET_NULL, null=True, blank=True)
    images = models.ImageField(upload_to="images/sliders_images")


class Genre(models.Model):

    """
    Модель, отображающая жанр книги( Научная фантастика и др.)
    """

    name = models.CharField(max_length=200, help_text="Введите жанр книги (Например, Научная Фантастика, Французская Поэзия и др.)")

    def __str__(self):

        """
        Строка для отображения модели Объекта
        """

        return self.name


class Language(models.Model):
    """
    Модель, отображающая язык (Например Русский, Английский, Японский и т.д.)
    """

    name = models.CharField(max_length=200, help_text='Введите оригинальный язык книги (Например Русский, Английский и т.д.)')

    def __str__(self):

        return self.name


class Attachment(models.Model):

    def get_upload_to(instance, filename):

        return 'images/sliders_images/{0}/{1}'.format(Book.id, filename)

    image = models.ImageField(upload_to=get_upload_to, null=True)

    def __str__(self):

        """
        Строка для отображения модели Объекта
        """

        return self.name


class Book(models.Model):

    """
    Модель, отображающая книгу(но не конкретную копию книги).
    """

    title = models.CharField(max_length=200)
    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True)
    # Foreign Key используется потому что у книги может быть один автор, но у автора может быть много книг
    # Автор как строка, а не объект, потому что он еще не объявлен в файле.
    summary = models.TextField(max_length=1000, help_text='Введите описание книги')
    benefits = models.CharField(max_length=1000, help_text='Введите краткое описание книги. Каждую строку отделять ,', null=True)
    thumbnail = models.ImageField(upload_to='images/thumbnails', default="images/thumbnails/default.jpg")
    images = models.ForeignKey(Property, help_text='Загрузите фотографии для слайдера', on_delete=models.CASCADE, null=True, blank=True)
    isbn = models.CharField('ISBN', max_length=13, help_text='13 Character <a href="https://www.isbn-international.org/content/what-isbn">ISBN number</a>')
    genre = models.ManyToManyField(Genre, help_text='Выберите жанр для книги')
    # ManyToManyField используется потому что жанр может содержать множество книг. Книги могут относиться к многим жанрам.
    # Класс жанра уже определен, поэтому мы можем указать объект выше.
    language = models.ForeignKey('Language', on_delete=models.SET_NULL, null=True)
    price = models.IntegerField(null=True)

    def __str__(self):

        return self.title

    def get_absolute_url(self):

        """
        Возвращает URL-адрес для доступа к конкретному экземпляру книги.
        """

        return reverse('book_detail', args=[str(self.id)])

    def display_genre(self):
        """
        Создаёт строку для жанра. Это необходимо для отображения жанра в Админ-панели
        """
        return ', '.join([genre.name for genre in self.genre.all()[:3]])

    display_genre.short_description = 'Жанр'

    def get_images_list(self):

        property = Property.objects.get(address=self.images)
        images_list = property.images.all()
        return images_list

    def get_benefits_list(self):

        try:
            return self.benefits.split(',')
        except AttributeError:
            return ['Этот раздел необходимо заполнить']


class BookInstance(models.Model):

    """
    Модель, представляющая конкретную копию книги (Т.е. которая может быть заимствована из библиотеки).
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Уникальный идентификатор для этой конкретной книги по всей библиотеке')
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True)
    imprint = models.CharField(max_length=200)
    due_back = models.DateField(null=True, blank=True)
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    LOAN_STATUS = (
        ('m', 'На обслуживании'),
        ('o', 'Выдана'),
        ('a', 'Доступна'),
        ('r', 'Зарезервирована'),
    )

    status = models.CharField(max_length=1, choices=LOAN_STATUS, blank=True, default='m', help_text='Статус книги')

    class Meta:
        ordering = ["due_back"]
        permissions = (("can_mark_returned", "Set book as returned"),)

    def __str__(self):

        return '{0} ({1})'.format(self.id, self.book.title)

    @property
    def is_overdue(self):
        if self.due_back and date.today() > self.due_back:
            return True
        return False


class Author(models.Model):

    """
    Модель, отображающая автора.
    """

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    date_of_death = models.DateField('died', null=True, blank=True)

    def get_absolute_url(self):

        return reverse("author_detail", args=[str(self.id)])


    def __str__(self):

        return '{0}, {1}'.format(self.last_name, self.first_name)


    class Meta:
        ordering = ['last_name']


class Merchandise(models.Model):

    """
    Модель, отображающая продукция.
    """

    title = models.CharField(max_length=100)
    price = models.IntegerField()
    description = models.TextField(max_length=1000)
    thumbnail = models.ImageField(upload_to='images/merchandise', default="images/merchandise/default.jpg")
    images = models.ForeignKey(Property, help_text='Загрузите фотографии для слайдера', on_delete=models.CASCADE, null=True, blank=True)

    def get_absolute_url(self):

        return reverse("merchandise_detail", args=[str(self.id)])


    def __str__(self):

        return self.title


    def get_images_list(self):

        property = Property.objects.get(address=self.images)
        images_list = property.images.all()
        return images_list



