# Generated by Django 2.1.3 on 2018-12-12 13:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0027_auto_20181212_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Author'),
        ),
        migrations.AlterField(
            model_name='book',
            name='images',
            field=models.ForeignKey(blank=True, help_text='Загрузите фотографии для слайдера', null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.Property'),
        ),
    ]
