# Generated by Django 2.1.3 on 2018-12-11 09:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0023_auto_20181211_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='propertyimage',
            name='image',
            field=models.ImageField(upload_to='images/sliders_images'),
        ),
    ]
