from django.conf.urls import url
from django.urls import path

from . import views


urlpatterns = [
    url(r'^books/$', views.BookListView.as_view(), name='books'),
    url(r'^books/rosman/$', views.BooksRosman.as_view(), name='books_rosman'),
    url(r'^books/eng/$', views.BooksEng.as_view(), name='books_eng'),
    url(r'^books/textbooks/$', views.BooksTextbooks.as_view(), name='books_textbooks'),
    url(r'^book/(?P<pk>\d+)$', views.BookDetailView.as_view(), name='book_detail'),
    url(r'^book/(?P<pk>\d+)/update/$', views.BookUpdate.as_view(), name='book_update'),
    url(r'^book/create/$', views.BookCreate.as_view(), name='book_create'),
    url(r'^book/(?P<pk>\d+)/delete/$', views.BookDelete.as_view(), name='book_delete'),
    url(r'^book/(?P<pk>\d+)/add_to_cart/$', views.add_to_cart, name='add_to_cart'),
    url(r'^merch/(?P<pk>\d+)/add_to_cart/$', views.add_to_cart, name='add_to_cart_merch'),
    #######
    path('merch/<int:pk>', views.MerchandiseDetailView.as_view(), name='merchandise_detail'),
    url(r'^merch/standart-sticks/$', views.MagicStickStandart.as_view(), name='merch_stand_stick'),
    url(r'^merch/metal-sticks/$', views.MagicStickMetal.as_view(), name='merch_metal_stick'),
    url(r'^merch/clothes/$', views.MerchClothes.as_view(), name='merch_clothes'),
    url(r'^merch/hats/$', views.MerchClothesHats.as_view(), name='merch_clothes_hats'),
    url(r'^merch/scarves/$', views.MerchClothesScarves.as_view(), name='merch_clothes_scarves'),
    url(r'^merch/gloves/$', views.MerchClothesGloves.as_view(), name='merch_clothes_gloves'),
    url(r'^merch/ties/$', views.MerchClothesTies.as_view(), name='merch_clothes_ties'),
    url(r'^merch/caps/$', views.MerchClothesCaps.as_view(), name='merch_clothes_caps'),
    url(r'^merch/sweaters/$', views.MerchClothesSweaters.as_view(), name='merch_clothes_sweaters'),
    url(r'^merch/mantle/$', views.MerchClothesMantle.as_view(), name='merch_clothes_mantle'),
    url(r'^merch/socks/$', views.MerchClothesSocks.as_view(), name='merch_clothes_socks'),
    url(r'^merch/tshirt/$', views.MerchClothesTshirts.as_view(), name='merch_clothes_tshirts'),
    url(r'^merch/tshirt/$', views.MerchClothesTshirts.as_view(), name='merch_clothes_tshirts'),
    ######
    url(r'^merch/accessories/$', views.MerchAccessories.as_view(), name='merch_accessories'),
    url(r'^merch/accessories/figurines/$', views.MerchAccessoriesFigurines.as_view(), name='merch_accessories_figurines'),
    url(r'^merch/accessories/pendants/$', views.MerchAccessoriesPendants.as_view(), name='merch_accessories_pendants'),
    url(r'^merch/accessories/notebooks/$', views.MerchAccessoriesNotebooks.as_view(), name='merch_accessories_notebooks'),
    url(r'^merch/accessories/bracelet/$', views.MerchAccessoriesBracelet.as_view(), name='merch_accessories_bracelet'),
    url(r'^merch/accessories/wallet/$', views.MerchAccessoriesWallet.as_view(), name='merch_accessories_wallet'),
    url(r'^merch/accessories/bookmark/$', views.MerchAccessoriesBookmark.as_view(), name='merch_accessories_bookmark'),
    url(r'^merch/accessories/rings/$', views.MerchAccessoriesRings.as_view(), name='merch_accessories_rings'),
    url(r'^merch/accessories/mugs/$', views.MerchAccessoriesMugs.as_view(), name='merch_accessories_mugs'),
    url(r'^merch/accessories/watches/$', views.MerchAccessoriesWatches.as_view(), name='merch_accessories_watches'),
    url(r'^merch/accessories/pencil_cases/$', views.MerchAccessoriesPencilCases.as_view(), name='merch_accessories_pencil_cases'),
    ######
    url(r'^author/create/$', views.AuthorCreate.as_view(), name='author_create'),
    url(r'^author/(?P<pk>\d+)/update/$', views.AuthorUpdate.as_view(), name='author_update'),
    url(r'^author/(?P<pk>\d+)/delete/$', views.AuthorDelete.as_view(), name='author_delete'),
    ######
    url(r'^cart/$', views.get_cart, name="cart"),
    url(r'^cart/clear', views.clear_cart, name="clear_cart"),
    url(r'^cart/(?P<pk>\d+)/delete/$', views.remove_from_cart, name="delete_item"),
    url(r'^cart/complete', views.cart_complete_order, name="cart_complete_order"),
    ######
    path('/search', views.SearchView.as_view(), name='search_view'),
]
