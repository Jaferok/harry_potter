from .models import Book, Author, BookInstance, Genre, Merchandise
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse
from django.views import generic
from django.db.models import Q
from itertools import chain
from cart.cart import Cart
from random import sample
import datetime

from .forms import RenewBookForm


class BookListView(generic.ListView):
    model = Book
    paginate_by = 10

    def get_context_data(self, **kwargs):
        # В первую очередь получаем базовую реализацию контекста
        context = super(BookListView, self).get_context_data(**kwargs)
        # Добавляем новую переменную к контексту и иниуиализируем ее некоторым значением
        context['some_data'] = 'This is just some data'
        return context


class BookDetailView(generic.DetailView):

    model = Book

    def get_context_data(self, **kwargs):

        context = super(BookDetailView, self).get_context_data(**kwargs)
        cart = Cart(self.request)
        items = Book.objects.filter(pk=self.kwargs['pk'])
        like_main_items_books = Book.objects.all().exclude(pk=self.kwargs['pk'])
        like_main_items_merch = Merchandise.objects.all()
        like_main_items_all = list(chain(like_main_items_books, like_main_items_merch))

        context['random_choice'] = sample(like_main_items_all, 5)
        context['cart'] = cart
        context['items'] = items
        return context


class AuthorListView(generic.ListView):

    model = Author
    paginate_by = 10


class AuthorDetailView(generic.DetailView):

    model = Author


class AuthorCreate(CreateView):

    model = Author
    fields = "__all__"


class AuthorUpdate(UpdateView):

    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']


class AuthorDelete(DeleteView):

    model = Author
    success_url = reverse_lazy('authors')


class BookCreate(CreateView):

    model = Book
    fields = "__all__"


class BookUpdate(UpdateView):

    model = Book
    fields = "__all__"


class BookDelete(DeleteView):

    model = Book
    success_url = reverse_lazy('authors')


class MerchandiseDetailView(generic.DetailView):

    model = Merchandise

    def get_context_data(self, **kwargs):

        context = super(MerchandiseDetailView, self).get_context_data(**kwargs)
        cart = Cart(self.request)
        items = Merchandise.objects.filter(pk=self.kwargs['pk'])
        like_main_items_merch = Merchandise.objects.all().exclude(pk=self.kwargs['pk'])
        like_main_items_books = Book.objects.all()
        like_main_items_all = list(chain(like_main_items_books, like_main_items_merch))

        context['random_choice'] = sample(like_main_items_all, 5)
        context['cart'] = cart
        context['items'] = items
        return context


class SearchView(generic.View):

    template_name = 'catalog/search_results.html'

    def get(self, request, *args, **kwargs):

        query = request.GET.get('q')
        founded_books = Book.objects.filter(
            Q(title__icontains=query) |
            Q(summary__icontains=query) |
            Q(author__first_name__icontains=query) |
            Q(author__last_name__icontains=query)
        )

        founded_merch = Merchandise.objects.filter(
            Q(title__icontains=query) |
            Q(description__icontains=query)
        )

        results = list(chain(founded_books, founded_merch))


        paginator = Paginator(results, 20)

        page = request.GET.get('page')
        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        return render(
            request,
            'catalog/search_results.html',
            {
                'results': results,
                'count': paginator.count
            }
        )


def add_to_cart(request, pk):

    if request.method == "POST" and request.POST.get('quantity'):

        response_data = {}

        quantity = request.POST.get('quantity')
        count = request.POST.get('count')
        merch = request.POST.get("merch")

        if merch == "True":
            product = Merchandise.objects.get(id=pk)
        else:
            product = Book.objects.get(id=pk)

        cart = Cart(request)
        cart.add(product, product.price, count)
        items_number = cart.count()

        response_data['items_number'] = items_number
        response_data['count'] = str(int(quantity) + 1)
        response_data['summary'] = cart.summary()
        response_data['item_total_price'] = product.price * int(response_data['count'])
        response_data['is_success'] = True
        response_data['item'] = merch

        return JsonResponse(response_data)

    elif request.method == "POST":

        response_data = {}

        quantity = request.POST.get('count')
        merch = request.POST.get("merch")

        if merch == "True":
            product = Merchandise.objects.get(id=pk)
        else:
            product = Book.objects.get(id=pk)

        cart = Cart(request)
        cart.add_button(product, product.price, quantity)
        items_number = cart.count()

        response_data['items_number'] = items_number
        response_data['count_py'] = int(quantity)
        response_data['summary'] = cart.summary()
        response_data['item_total_price'] = product.price * int(quantity)
        response_data['is_success'] = True
        response_data['item'] = merch

        return JsonResponse(response_data)

    else:
        count = request.POST.get('count')
        if request.POST.get('item') == "merchandise":
            product = Merchandise.objects.get(id=pk)
        else:
            product = Book.objects.get(id=pk)
        cart = Cart(request)
        cart.add(product, product.price, count)

        return render(request, 'catalog/add_to_cart.html', dict(cart=Cart(request)))


def remove_from_cart(request, pk):

    if request.method == "POST":

        response_data = {}

        quantity = request.POST.get('quantity')
        count = request.POST.get('count')
        product = Book.objects.get(id=pk)
        cart = Cart(request)
        cart.delete(product, product.price, count)
        items_number = cart.count()

        response_data['items_number'] = items_number
        response_data['count'] = str(int(quantity) - 1)
        response_data['summary'] = cart.summary()
        response_data['item_total_price'] = product.price * int(response_data['count'])
        response_data['is_success'] = True

        return JsonResponse(response_data)

    else:

        product = Book.objects.get(id=pk)
        cart = Cart(request)
        cart.remove(product)

        return render(request, 'catalog/cart.html', dict(cart=Cart(request)))


def get_cart(request):
    return render(request, 'catalog/cart.html', dict(cart=Cart(request)))


def clear_cart(request):

    cart = Cart(request)
    cart.clear()
    return render(request, 'catalog/cart.html', dict(cart=Cart(request)))


def cart_complete_order(request):

    return render(request, 'catalog/cart_complete_order.html', dict(cart=Cart(request)))


class BooksRosman(generic.ListView):

    model = Book
    paginate_by = 10
    template_name = 'catalog/book_list.html'

    def get_queryset(self):
        return Book.objects.filter(benefits__icontains='РОСМЭН').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(BooksRosman, self).get_context_data(**kwargs)
        title = "Книги Гарри Поттер РОСМЭН"

        context['title'] = title
        return context


class BooksEng(generic.ListView):

    model = Book
    paginate_by = 10
    template_name = 'catalog/book_list.html'

    def get_queryset(self):
        return Book.objects.filter(benefits__icontains='Английский').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(BooksEng, self).get_context_data(**kwargs)
        title = "Книги Гарри Поттер на английском языке"

        context['title'] = title
        return context


class BooksTextbooks(generic.ListView):

    model = Book
    paginate_by = 10
    template_name = 'catalog/book_list.html'

    def get_queryset(self):
        return Book.objects.filter(benefits__icontains='Учебники Хогвартса').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(BooksTextbooks, self).get_context_data(**kwargs)
        title = "Книги Гарри Поттер серия неофициальных книг Учебники Хогвартса"

        context['title'] = title
        return context


class MagicStickStandart(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(description__icontains='стандартным').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MagicStickStandart, self).get_context_data(**kwargs)
        title = "Волшебные палочки со стандартным стержнем"

        context['title'] = title
        return context


class MagicStickMetal(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(description__icontains='металлическим').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MagicStickMetal, self).get_context_data(**kwargs)
        title = "Волшебные палочки со металлическим стержнем"

        context['title'] = title
        return context


class MerchClothes(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(description__icontains='материал').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothes, self).get_context_data(**kwargs)
        title = "Одежда Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesHats(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='шапка').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesHats, self).get_context_data(**kwargs)
        title = "Шапки Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesScarves(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='шарф').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesScarves, self).get_context_data(**kwargs)
        title = "Шарфы Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesGloves(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='перчатки').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesGloves, self).get_context_data(**kwargs)
        title = "Перчатки Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesTies(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='галстук').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesTies, self).get_context_data(**kwargs)
        title = "Галстуки Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesCaps(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='кепка').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesCaps, self).get_context_data(**kwargs)
        title = "Кепки Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesSweaters(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='свитер').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesSweaters, self).get_context_data(**kwargs)
        title = "Кофты Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesMantle(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='мантия').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesMantle, self).get_context_data(**kwargs)
        title = "Мантии Гарри Поттер"

        context['title'] = title
        return context


class MerchClothesSocks(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='носки').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesSocks, self).get_context_data(**kwargs)
        title = "Носки Гарри Поттер"

        context['title'] = title
        return context



class MerchClothesTshirts(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='футболка').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchClothesTshirts, self).get_context_data(**kwargs)
        title = "Футболки Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessories(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(description__icontains='аксессуар').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessories, self).get_context_data(**kwargs)
        title = "Аксессуары Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesFigurines(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='Фигурка').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesFigurines, self).get_context_data(**kwargs)
        title = "Фигурки Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesPendants(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='кулоны').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesPendants, self).get_context_data(**kwargs)
        title = "Кулоны Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesNotebooks(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='Блокнот').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesNotebooks, self).get_context_data(**kwargs)
        title = "Мантии Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesBracelet(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='Браслет').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesBracelet, self).get_context_data(**kwargs)
        title = "Браслеты Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesWallet(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):

        wallet_qs = Merchandise.objects.filter(title__icontains='кошелёк').order_by('title')
        purse_qs = Merchandise.objects.filter(title__icontains='портмоне').order_by('title')

        union_qs = wallet_qs.union(purse_qs)

        return union_qs

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesWallet, self).get_context_data(**kwargs)
        title = "Бумажники Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesBookmark(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='закладка').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesBookmark, self).get_context_data(**kwargs)
        title = "Закладки Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesRings(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='кольцо').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesRings, self).get_context_data(**kwargs)
        title = "Кольца Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesMugs(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='кружка').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesMugs, self).get_context_data(**kwargs)
        title = "Кружки Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesWatches(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='Часы').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesWatches, self).get_context_data(**kwargs)
        title = "Наручные часы Гарри Поттер"

        context['title'] = title
        return context


class MerchAccessoriesPencilCases(generic.ListView):

    model = Merchandise
    paginate_by = 10
    template_name = 'catalog/merchandise_list.html'

    def get_queryset(self):
        return Merchandise.objects.filter(title__icontains='Пенал').order_by('title')

    def get_context_data(self, **kwargs):

        context = super(MerchAccessoriesPencilCases, self).get_context_data(**kwargs)
        title = "Пеналы Гарри Поттер"

        context['title'] = title
        return context
