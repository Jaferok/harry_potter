from django.db import models


class Sales(models.Model):

    """
    Модель, отображающая акции.
    """

    title = models.CharField(max_length=250)
    description = models.TextField(max_length=1000)
    image = models.ImageField(upload_to='images/sales')

    def __str__(self):

        return self.title


class Review(models.Model):

    """
    Модель, отображающая отзывы.
    """

    name = models.CharField(max_length=100)
    review = models.TextField(max_length=500)
    avatar = models.ImageField(upload_to='images/reviews')

    def __str__(self):

        return self.name
