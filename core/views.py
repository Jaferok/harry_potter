from .models import Sales, Review
from catalog.models import Book, Merchandise
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse
from django.views import generic
from django.db.models import Q
from itertools import chain
from cart.cart import Cart
from random import sample
import datetime


def index(request):
    """
    Функция отображения для домашней страницы сайта.
    """
    # Генерация кол-тв некоторых главных объектов
    available_books = Book.objects.all()
    available_merchandise = Merchandise.objects.all()
    cart = Cart(request)

    # Отрисовка HTML-шаблона index.html с данными внутри
    # переменной контекста context
    return render(
        request,
        'index.html',
        context={
            'available_books': available_books,
            'available_merchandise': available_merchandise,
            'cart': cart,
        },
    )


def delivery(request):

    return render(
        request,
        'delivery.html'
    )


def sales(request):

    sales = Sales.objects.all()

    return render(
        request,
        'sales.html',
        context={
            'sales': sales
        }
    )


def reviews(request):

    reviews = Review.objects.all()

    return render(
        request,
        'reviews.html',
        context={
            'reviews': reviews
        }
    )


def contacts(request):

    return render(
        request,
        'contacts.html'
    )
